var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Red5Pro = youbora.StandardAdapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.playhead
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    if (this.flags.isPaused) return 0
    return this.tagplayer ? this.tagplayer.playbackRate : null
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    return this.tagplayer ? this.tagplayer.webkitDroppedFrameCount : null
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return this.bitrate
  },

  /** Override to return rendition */
  getRendition: function () {
    if (this.tagplayer) {
      return youbora.Util.buildRenditionString(this.tagplayer.videoWidth, this.tagplayer.videoHeight, this.bitrate)
    }
    return this.bitrate
  },

  /** Override to return title */
  getTitle: function () {
    return this.player.getOptions().streamName
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return true
  },

  /** Override to return resource URL. */
  getResource: function () {
    var connection = this.player.getConnection()
    if (connection && connection._websocket) {
      return connection._websocket.url
    }
    return 'unknown'
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return this.isSubscriber() ? 'Red5Pro-subscriber' : 'Red5Pro-publisher'
  },

  initializeAdapter: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false, 1200)  //No seek for this case!
  },

  getListenersList: function () {
    if (this.isSubscriber()) {
      var events = red5prosdk.SubscriberEventTypes
      return [{
        events: {
          [events.CONNECT_SUCCESS]: this.playListener,
          [events.PLAYBACK_STATE_CHANGE]: this.stateChangeListener,
          [events.PLAYBACK_TIME_UPDATE]: this.timeupdateListener,
          [events.CONNECTION_CLOSED]: this.stopListener,
          [events.CONNECT_FAILURE]: this.errorListener,
          [events.FULL_SCREEN_STATE_CHANGE]: null,
          [events.ORIENTATION_CHANGE]: null,
          [events.PLAY_UNPUBLISH]: null,
          [events.SUBSCRIBE_FAIL]: null,
          [events.SUBSCRIBE_STOP]: null,
          [events.SUBSCRIBE_START]: null,
          [events.SUBSCRIBE_SEND_INVOKE]: null,
          [events.SUBSCRIBE_METADATA]: this.metadataListener,
          [events.SUBSCRIBE_INVALID_NAME]: this.errorListener
        }
      }]
    } else { // isPublisher
      //var events = red5prosdk.PublisherEventTypes
      youbora.Log.error("Adapter not working for Publisher")
      return null
    }
  },

  playListener: function (e) {
    this.fireStart()
  },

  timeupdateListener: function (e) {
    this.playhead = e.data.time
    if (this.playhead > 0.25) {
      this.fireStart()
      this.fireJoin()
      if (!this.tagplayer) {
        this.tagplayer = this.player.getPlayer()
      }
    }
  },

  stateChangeListener: function (e) {
    switch (e.data.state) {
      case 'Playback.IDLE':
        this.flags.isPaused ? this.fireStop() : this.fireStop({ 'pauseDuration': -1 })
        break
      case 'Playback.PAUSED':
        this.cancelBuffer()
        this.firePause()
        break
      case 'Playback.PLAYING':
        this.fireResume()
        this.monitor.skipNextTick()
        break
    }
  },

  errorListener: function (e) {
    this.fireError(e.type)
    this.fireStop()
  },

  stopListener: function (e) {
    if (this.flags.isPaused) {
      this.fireStop()
    } else {
      this.fireStop({ 'pauseDuration': -1 })
    }
  },

  metadataListener: function (e) {
    this.bitrate = 0
    if (e.data.videoBR) {
      this.bitrate += e.data.videoBR
    }
    if (e.data.audioBR) {
      this.bitrate += e.data.audioBR
    }
    this.bitrate *= 1000 // kbps to bps
  },

  isSubscriber: function (e) {
    return !!this.player.pause
  }
})

module.exports = youbora.adapters.Red5Pro
